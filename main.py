from help import *
from collections import namedtuple
from dataclasses import dataclass
from typing import List


frame = namedtuple('frame', ['tag_name', 'size', 'flags', 'data'])
mheader = namedtuple('mheader', ['file_identifier', 'version', 'flags', 'size'])


def get_id3_size(bytes_sequences: bytes) -> int:
    mask = 255
    int_bytes = int.from_bytes(bytes_sequences, byteorder='big')
    result = (int_bytes & (mask << 24)) >> 3
    for shift in reversed(range(1, 4)):
        shifted_mask = mask << (8 * (shift - 1))
        mask_on_bytes = (int_bytes & shifted_mask) >> (shift - 1)
        result = result | mask_on_bytes
    return result


def search_frames(id3_bytes: bytes, id3_size: int) -> List[frame]:
    start = 0
    end = 4
    frames = []

    while start != id3_size:
        tag_name = id3_bytes[start:end]
        if tag_name in id3_frames_names:
            size = int.from_bytes(id3_bytes[end:end+4], byteorder='big')
            flags = id3_bytes[end+4:end+6]
            data = id3_bytes[start+10:start+10+size]

            frames.append(frame(tag_name, size, flags, data))

            start += 10 + size
            end += 10 + size
        else:
            raise Exception("Error tag read")

    return frames


def main():

    img_name = "image.png"
    img = open(img_name, 'rb')

    ibytes = img.read()

    file_name = "Crystal Castles Love and Caring.mp3"
    file = open(file_name, 'rb')

    ten_bytes = file.read(10)

    id3_main_header = mheader(ten_bytes[:3], ten_bytes[3:5], ten_bytes[5], ten_bytes[6:])

    id3_bytes_number = get_id3_size(id3_main_header.size)

    id3_bytes = file.read(id3_bytes_number)
    frames = search_frames(id3_bytes, id3_bytes_number)

    print('asd')


if __name__ == "__main__":
    main()
